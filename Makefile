# Note: Comments are unindented to not be printed during the installation

.PHONY: install uninstall

install:
# This command verifies if the group _xd exists. If it doesn't, it creates
# it with gid 839.
	@if ! groupinfo -e _xd; then \
		echo "Creating _xd group"; \
		groupadd -g 839 _xd; \
	fi

# These commands verify if the user _xd exists. If it doesn't, it creates
# it with uid 839, default group 839 (which is _xd), comment "XD account",
# home directory /var/lib/xd, and login shell /sbin/nologin (which forbids
# it from loging in). The mkdir and chown are used instead of the -m option
# of useradd in case /var/lib/xd already existed before the execution of
# this make target.
	@if ! userinfo -e _xd; then \
		echo "Creating _xd user"; \
		mkdir -p /var/lib/xd; \
		useradd -u 839 -g 839 -c "XD account" \
		-d /var/lib/xd -s /sbin/nologin _xd; \
		chown -R _xd:_xd /var/lib/xd; \
	fi

# These commands make the xd executable belong to the _xd user and
# group, and make it non-writable for all and non-executable for others.
	@echo "Giving ownership of xd to _xd"
	@chown _xd:_xd $$(which XD)
	@chmod 550 $$(which XD)

# This command copies the RC script in the RC script folder to allow XD to
# be managed by the service supervisor.
	@echo "Installing xd service"
	@install -o root -g bin -m 555 xd /etc/rc.d/xd



uninstall:
# This command removes the RC script.
	@echo "Removing xd service"
	@rcctl disable xd
	@rm -f /etc/rc.d/xd

# These commands restore the ownership and permissions of xd.
	@echo "Giving ownership of xd back to root"
	@chown root:wheel $$(which XD)
	@chmod 755 $$(which XD)

# This command removes the _xd user and its home if they exist.
	@if userinfo -e _xd; then \
		echo "Removing _xd user"; \
		userdel -r _xd; \
	fi

# This command removes the _xd group if it exists.
	@if groupinfo -e _xd; then \
		echo "Removing _xd group"; \
		groupdel _xd; \
	fi


