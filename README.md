# XD RC script for OpenBSD

Instructions on how to compile the XD bittorrent client on OpenBSD, together
with a makefile and an RC script to make it a system service, allowing it to be
started at boot and so on.


## Description

[XD](https://xd-torrent.github.io/) is a P2P file-sharing application that uses
the BitTorrent protocol and works exclusively on the
[I2P network](https://geti2p.net/en/), preventing the user's IP to be leaked
because of a proxy or VPN misconfiguration. It is straightforward to compile and
to install, and offers both a CLI and a web interface.

This repository contains instructions on how to compile and use XD on OpenBSD,
focusing on the web interface, together with a makefile and an RC script to make
it a system service, allowing it to be started at boot, restarted if it
crashes, managed by `rcctl`, etc.


## Compiling and installing

This part is taken from
[the official instructions](https://xd-torrent.readthedocs.io/en/latest/user-guide/install/#building-from-source)
for compiling and installing XD.
If the instructions listed here differ from the official instructions,
prioritize the official instructions, unless a detail specific to OpenBSD is
stated here.

Of course, since XD works exclusively on the I2P network, you need an I2P proxy
to be able to use it. You can have a look at
[this repository](https://gitlab.com/alpou/i2pd-rc-openbsd) for instructions on
how to compile I2Pd on OpenBSD and make it a system service. The SAM bridge
needs to be enabled on the I2P router for XD to use it.

First, install
[a few dependencies](https://xd-torrent.readthedocs.io/en/latest/user-guide/install/#dependencies).
Git is not listed as a dependency in the official documentation, but it is
needed as it does not come with OpenBSD's base system:

    $ doas pkg_add git gmake go

Next, clone their git repository:

    $ cd /a/folder/of/your/choice/where/to/clone/the/repo
    $ git clone https://github.com/majestrate/XD

Then, compile the source. You need to use `gmake` instead of `make` to use GNU
Make on OpenBSD:

    $ cd XD
    $ gmake

Next, install the binaries you just compiled:

    $ doas gmake install PREFIX=/usr/local


## Making XD start at boot

At this point, you can start XD manually, but it won't start automatically
when the system boots. This section describes how to achieve this.

The first thing to do is to clone this git repository:

    $ cd /a/folder/of/your/choice/where/to/clone/the/repo
    $ git clone https://gitlab.com/alpou/xd-rc-openbsd.git

Then, simply run the makefile. This step creates a dedicated `_xd` user and a
group of the same name if they don't already exist, makes the binaries
previously compiled belong to them, and copies the init script in `/etc/rc.d`.

    $ cd xd-rc-openbsd
    $ doas make install

Next, start the service once. This will generate a default configuration
file at `/etc/xd/torrents.ini`.

    $ doas rcctl start xd

You can now edit the config file to your liking. Note that by default,
starting XD also creates a directory called `storage` and a file called
`trackers.ini` in the current directory. A good idea is to change their
location and to specify absolute paths. Here are example snippets of
`/etc/xd/torrents.ini` to do just that:

    [storage]
    rootdir=/home/example/storage
    metadata=/home/example/storage/metadata
    downloads=/home/example/storage/downloads
    completed=/home/example/storage/seeding
    
    ...
    
    [bittorrent]
    ...
    tracker-config=/etc/xd/trackers.ini

Note that the directory containing the torrents (`/home/example/storage` in the
example config file above) must be readable and writable by the `_xd` user. A
good way to achieve this is to make the `_xd` group own the directory. Following
the example above, the following command would need to be executed:

    $ doas chown -R :_xd /home/example/storage

In the config file, `address` in the `[i2p]` section defines the address and
port of the SAM bridge of your I2P router where XD will connect. Also, `bind` in
the `[rpc]` section defines the address and port to access XD's web interface,
if you choose to use it instead of the command line interface.


The final step is to enable the service to make it start at boot time and then
to manually restart it to take into account your new configuration:

    $ doas rcctl enable xd
    $ doas rcctl restart xd

Enjoy!


## Licensing

The content of this repository is provided under a 0BSD license. See the LICENSE
file for more information.
